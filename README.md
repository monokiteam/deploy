# Deploy Scripts for MONOKI specific deployments

These scripts give you some tools for local DEV environment to update your local db an upload a dump from your DEV-env 
to the "__data" folder in your PROD-Env.

It includes the bitbucket-pipelines.yml file what overwrites your local file, whenever you update your composer 
installation.

The script will install env files downloaded from the amazeeio/lagoon repo.
https://github.com/amazeeio/lagoon/tree/master/docs/using_lagoon/drupal/drupal8-composer-mariadb

## Install
Put/Update these vars in your docker-compose.yml
```
      ENV_SSH_PORT: "-p222"
      ENV_SSH_ADDRESS: "myhost@dedi5559.your-server.de"
      ENV_PROD_PATH: "/usr/home/myhost/public_html/my-domain.de/"
```

Now you can update your DB from DRUPAL_ROOT with:
```$xslt
$ ../scripts/deploy/syncdb

// Or upload your local db with:

$ ../scripts/deploy/uploaddb

```

To auto-update your bitbucket pipeline add these lines to your composer.json and update.

```$xslt

    "repositories": [
        ...,
        {
            "type": "vcs",
            "url": "git@bitbucket.org:monokiteam/deploy.git"
        }
    },
    "autoload": {
        "psr-4": {
            ... ,
            "MonokiTeam\\deploy\\": "scripts/deploy/"
        }
    },
    "scripts": {
        ...
        "post-update-cmd": [
            ... ,
            "MonokiTeam\\deploy\\ScriptHandler::createRequiredFiles"
        ]
    },
    "extra": {
        "installer-types": [... , "drupal-scripts"],
        "installer-paths": {
            ...
            "scripts/{$name}": ["type:drupal-scripts"]
        },


```

## Bitbucket config
Bitbucket needs the following ENV variables.
https://bitbucket.org/monokiteam/my-domain.de/admin/addon/admin/pipelines/repository-variables

```$xslt
DOCROOT 'docroot'
PROD_PATH '/usr/home/USERNAME/public_html/my-domain.de/'
STAGE_PATH '/usr/home/USERNAME/public_html/stage.my-domain.de/'
SSH_USER 'USERNAME'
SSH_HOST 'dedi5559.your-server.com -p222'
MAJOR_RELEASE 'v1'
```
