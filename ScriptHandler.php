<?php
/**
 * @file
 * Contains \DrupalProject\composer\ScriptHandler.
 */
namespace MonokiTeam\deploy;

use Composer\Script\Event;
use Composer\Semver\Comparator;
use DrupalFinder\DrupalFinder;
use Symfony\Component\Filesystem\Filesystem;
use Webmozart\PathUtil\Path;


class ScriptHandler {

  public static function createRequiredFiles(Event $event) {
    $fs = new Filesystem();
    $drupalFinder = new DrupalFinder();
    $drupalFinder->locateRoot(getcwd());
    $composerRoot = $drupalFinder->getComposerRoot();


    // Update if exists (Optional in a project).
    $update = [
      'composer.phar',
      'bitbucket-pipelines.yml'
    ];
    foreach($update as $filename) {
      if (
        $fs->exists($composerRoot . '/' . $filename)
        and $fs->exists($composerRoot . '/scripts/deploy/env_files/' . $filename)
      ) {
        $event->getIO()->write("Create a $filename with chmod 0644");

        $fs->copy($composerRoot . '/scripts/deploy/env_files/' . $filename, $composerRoot . '/' . $filename);
        $fs->chmod($composerRoot . '/' . $filename, 0644);
        $event->getIO()->write("Create a $filename file with chmod 0644");
      }
    }

    // Copy if not exists (protect customizations).
    $update = [
      'drush/drush.yml',
      '.ddev/docker-compose.environment.yaml'
    ];
    foreach($update as $filename) {
      if (
        !$fs->exists($composerRoot . '/' . $filename)
        and $fs->exists($composerRoot . '/scripts/deploy/env_files/' . $filename)
      ) {
        $event->getIO()->write("Create a $filename with chmod 0644");

        $fs->copy($composerRoot . '/scripts/deploy/env_files/' . $filename, $composerRoot . '/' . $filename);
        $fs->chmod($composerRoot . '/' . $filename, 0644);
        $event->getIO()->write("Create a $filename file with chmod 0644");
      }
    }

    // Copy or override anyway.
    $mandatory = [
      '.ddev/homeadditions/.bash_aliases'
    ];
    foreach($mandatory as $filename) {
      if ($fs->exists($composerRoot . '/scripts/deploy/env_files/' . $filename)) {
        $event->getIO()->write("Create a $filename with chmod 0644");

        $fs->copy($composerRoot . '/scripts/deploy/env_files/' . $filename, $composerRoot . '/' . $filename);
        $fs->chmod($composerRoot . '/' . $filename, 0644);
        $event->getIO()->write("Create a $filename file with chmod 0644");
      }
    }
  }

}
